*** Settings ***
Resource           ${CURDIR}/Variable_Keywords.robot
Variables          ${CURDIR}/common.yaml
Library            BuiltIn
Library            String
Library            AppiumLibrary
Suite Setup        Open app
Suite Teardown     Close Application


*** Test Cases ***
Scenario : Validate add TODO
    Open app
    Click add Todo 
    Click input Title    
    On remind me
    Select Calendar
    Click year
    Swipe Up year
    Select year
    Select month
    Click submit Todo
    Check Todo complete

Scenario : Validate edit TODO
    Click Todo ListItem
    Off remind me
    Edit title
    Click submit Todo
    Check Edit Todo complete

Scenario : Validate delete TODO
    Slide delete todo 
