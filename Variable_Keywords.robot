*** Settings ***
Variables          ${CURDIR}/common.yaml
Library            BuiltIn
Library            String
Library            AppiumLibrary


*** Variable ***
#Setting
${Moreoptionbtn}    accessibility_id=More options
${checkboxbtn}      id=android:id/checkbox
${backbtn}          accessibility_id=Navigate up
${clickemail}       id=com.avjindersinghsekhon.minimaltodo:id/aboutContactMe

#Todo
${addtodo}              id=com.avjindersinghsekhon.minimaltodo:id/addToDoItemFAB
${userToDoEditText}     id=com.avjindersinghsekhon.minimaltodo:id/userToDoEditText
${makeToDo}             id=com.avjindersinghsekhon.minimaltodo:id/makeToDoFloatingActionButton
${TodoHasdate}          id=com.avjindersinghsekhon.minimaltodo:id/toDoHasDateSwitchCompat
${newTodoDateEditText}  id=com.avjindersinghsekhon.minimaltodo:id/newTodoDateEditText
${date_picker_year}     id=com.avjindersinghsekhon.minimaltodo:id/date_picker_year
${2023}                 accessibility_id=2023
${08November}           accessibility_id=08 November 2023






*** Keywords ***
Open app
    Open Application	  http://localhost:4723/wd/hub      platformName=android    platformVersion=10    deviceName=IRAQIJHAGEGAKNKN      app=/Users/pisitchuthongkum/Documents/GitHub/Minimal-Todo/app/app-release.apk


#Setting menu
Click more option
    Wait Until Element Is Visible    ${Moreoptionbtn}    timeout=10
    Click Element    ${Moreoptionbtn}
    sleep   0.5

Click setting button
    Sleep  0.5
    Click Text    Settings

Click about button
    Sleep  0.5
    Click Text    About

Click back button in setting page
    Wait Until Element Is Visible    ${backbtn}   timeout=10
    Click Element    ${backbtn} 

Click send email
    Click Element    ${clickemail}
    Press Keycode    4    metastate=None
    Press Keycode    4    metastate=None
    Sleep  0.5
    Wait Until Element Is Visible    ${backbtn}    timeout=20

Select Night mode
    Wait Until Element Is Visible    ${checkboxbtn}   timeout=10
    Click Element    ${checkboxbtn}
    Page Should Contain Text    Night mode is on
    ${checked}=    Get Element Attribute    class=android.widget.CheckBox    checkable
    Run Keyword If    '${checked}' == 'true'    Pass
    ...    ELSE IF   '${checked}' == 'false'    Fail

UnSelect Night mode
    Wait Until Element Is Visible    ${checkboxbtn}    timeout=10
    Click Element    ${checkboxbtn}
    Page Should Contain Text    Night mode is off
    ${checked}=    Get Element Attribute    class=android.widget.CheckBox    checkable
    Run Keyword If    '${checked}' == 'true'    Pass
    ...    ELSE IF   '${checked}' == 'false'    Fail


Pass 
    Should Match Regexp    true    true    msg=Pass    
    
Fail
    Should Match Regexp    true    true    msg=Fail    
    

#Add todo
Click add Todo 
    Wait Until Element Is Visible    ${addtodo}    timeout=10 
    Click Element    ${addtodo}

Click input Title       
    Wait Until Element Is Visible    ${userToDoEditText}    timeout=10 
    Page Should Contain Text    Title
    Input Text    ${userToDoEditText}    ${Title}

On remind me
    Wait Until Element Is Visible    ${TodoHasdate}       timeout=10
    Click Element    ${TodoHasdate} 
    Page Should Contain Text    Today

    #Check time + 1hour not complete
    #${hour} =	Get Time	hour min	NOW + 1 hour
    #${GetTime}=    Get Text 	 xpath=/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup[2]/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.EditText[2]
    #Log To Console    ${hour}    stream=STDOUT    no_newline=False 

    ${checked}=    Get Element Attribute    class=android.widget.Switch   checkable
    Run Keyword If    '${checked}' == 'true'    Pass
    ...    ELSE IF   '${checked}' == 'false'    Fail

Select Calendar
    Wait Until Element Is Visible    ${newTodoDateEditText}       timeout=10
    Click Element    ${newTodoDateEditText}

Click year
    Wait Until Element Is Visible    ${date_picker_year}       timeout=10
    Click Element    ${date_picker_year}
                 
Swipe Up year                         
    Wait Until Element Is Visible     id=com.avjindersinghsekhon.minimaltodo:id/animator
    ${element_size}=    Get Element Size    id=com.avjindersinghsekhon.minimaltodo:id/animator
    ${element_location}=    Get Element Location    id=com.avjindersinghsekhon.minimaltodo:id/animator
    ${start_x}=         Evaluate      ${element_location['x']} + (${element_size['width']} * 0.5)
    ${start_y}=         Evaluate      ${element_location['y']} + (${element_size['height']} * 0.7)
    ${end_x}=           Evaluate      ${element_location['x']} + (${element_size['width']} * 0.5)
    ${end_y}=           Evaluate      ${element_location['y']} + (${element_size['height']} * 0.3)
    Swipe               ${start_x}    ${start_y}  ${end_x}  ${end_y}  1500
    Sleep  1

Swipe Left to Right
    Wait Until Element Is Visible    id=com.avjindersinghsekhon.minimaltodo:id/listItemLinearLayout
    ${element_size}=    Get Element Size    id=com.avjindersinghsekhon.minimaltodo:id/listItemLinearLayout
    ${element_location}=    Get Element Location    id=com.avjindersinghsekhon.minimaltodo:id/listItemLinearLayout
    ${start_x}=         Evaluate      ${element_location['x']} + (${element_size['width']} * 0.3)
    ${start_y}=         Evaluate      ${element_location['y']} + (${element_size['height']} * 0.3)
    ${end_x}=           Evaluate      ${element_location['x']} + (${element_size['width']} * 0.5)
    ${end_y}=           Evaluate      ${element_location['y']} + (${element_size['height']} * 0.3)
    Swipe               ${start_x}    ${start_y}  ${end_x}  ${end_y}  1000
    Sleep  1


Select year    
    Wait Until Element Is Visible    ${2023}       timeout=10
    Click Element    ${2023}
    
Select month
    Swipe Up year
    Click Element    ${08November} 
    Click Text       OK
    Page Should Contain Text    8 Nov, 2023

Click submit Todo
    Wait Until Element Is Visible    ${makeToDo}     timeout=10
    Click Element    ${makeToDo} 

Check Todo complete
    Wait Until Element Is Visible    id=com.avjindersinghsekhon.minimaltodo:id/listItemLinearLayout    timeout=10
    Page Should Contain Text    Automate test

Check Edit Todo complete
    Wait Until Element Is Visible    id=com.avjindersinghsekhon.minimaltodo:id/listItemLinearLayout    timeout=10
    Page Should Contain Text    EditAutomate test

#Edit todo
Click Todo ListItem
    Click Text    Automate test
    
Off remind me
    Wait Until Element Is Visible    ${TodoHasdate}       timeout=10
    Click Element    ${TodoHasdate} 
    Sleep     0.5
    Page Should Not Contain Text    Today
    ${checked}=    Get Element Attribute    class=android.widget.Switch   checkable
    Run Keyword If    '${checked}' == 'true'    Pass
    ...    ELSE IF   '${checked}' == 'false'    Fail

Edit title
    Wait Until Element Is Visible    ${userToDoEditText}    timeout=10 
    Clear Text    ${userToDoEditText}
    Input Text    ${userToDoEditText}    ${EditTitle}

#Delete todo
Slide delete todo    
    Swipe Left to Right
    Sleep     2
    Wait Until Element Is Visible    id=com.avjindersinghsekhon.minimaltodo:id/toDoEmptyView    timeout=10
