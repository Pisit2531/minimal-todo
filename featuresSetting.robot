*** Settings ***
Resource           ${CURDIR}/Variable_Keywords.robot
Library            BuiltIn
Library            String
Library            AppiumLibrary
Suite Setup        Open app
Suite Teardown     Close Application



*** Test Cases ***

Scenario : Validate Open app
    Open app
    Wait Until Element Is Visible    id=com.avjindersinghsekhon.minimaltodo:id/toDoEmptyView    timeout=10
    
Scenario : Validate text in home page
    Page Should Contain Text    Minimal
    Page Should Contain Text    You don't have any todos

Scenario : Validate setting menu
    Click more option
    Page Should Contain Text    Settings	 
    Page Should Contain Text    About
    Click setting button
    Page Should Contain Text    Night Mode
    Page Should Contain Text    Night mode is off

Scenario : Validate Night Mode 
    Select Night mode
    UnSelect Night mode

Scenario : Validate setting page click back
    Click back button in setting page
    
Scenario : Validate About menu
    Click more option
    Page Should Contain Text    Settings
    Sleep  0.5
    Page Should Contain Text    About
    Click about button
    Page Should Contain Text    Made by Avjinder
    Sleep  0.5
    Page Should Contain Text    You can contact me at

Scenario : Validate access send email
    Click send email
    Click back button in setting page


    
    